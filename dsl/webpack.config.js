const HtmlWebPackPlugin = require("html-webpack-plugin");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const DashboardPlugin =require("@module-federation/dashboard-plugin");

const deps = require("./package.json").dependencies;
module.exports = {
  output: {
    publicPath: "http://localhost:8081/",
  },

  resolve: {
    extensions: [".tsx", ".ts", ".jsx", ".js", ".json"],
  },

  devServer: {
    port: 8081,
    historyApiFallback: true,
  },

  module: {
    rules: [
      {
        test: /\.m?js/,
        type: "javascript/auto",
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.(css|s[ac]ss)$/i,
        use: ["style-loader", "css-loader", "postcss-loader"],
      },
      {
        test: /\.(ts|tsx|js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
    ],
  },

  plugins: [
    new ModuleFederationPlugin({
      name: "dsl",
      filename: "remoteEntry.js",
      remotes: {},
      exposes: {
        "./Button":"./src/Button",
        "./ButtonFallback":"./src/ButtonFallback",
      },
      shared: [
        "styled-components",
        {
          react: {
            singleton: true
          },
          "react-dom": {
            singleton: true 
          }
        },
      ],
    }),
    new HtmlWebPackPlugin({
      template: "./src/index.html",
    }),
    new DashboardPlugin({
      publishVersion: require("./package.json").version,
      name:"dsl-Mirian",
      dashboardURL:
        "https://federation-dashboard-alpha.vercel.app/api/update?token=c31b8079-5d5c-45dc-81c6-117f6cafa2bc",
      metadata: {
        remote: "http://localhost:8081/remoteEntry.js",
      },
    }),
  ],
};
