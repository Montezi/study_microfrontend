<img src="https://images.unsplash.com/photo-1633356122544-f134324a6cee?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8cmVhY3QlMjBqc3xlbnwwfHwwfHw%3D&auto=format&fit=crop&w=4800&q=60"/>

# study_microfrontend
  Aplicação exemplo microfrontend utilizando module federation. 
  ###### (Aplicação desenvolvida na aula Micro Frontends Presente e Futuro - Experts Club - Rocketseat)


## Passos para execução

Instale as dependências dos projetos:

<img src="https://github.githubassets.com/images/icons/emoji/unicode/26a0.png" width="15px"/> usei o yarn para gerenciar os pacotes 
```
  cd dsl
``` 
```
  yarn install
```

```
  cd host
``` 
```
  yarn install
```
Em seguida inicie o servidor de desenvolvimento em cada aplicação:
```
yarn start
```
Após iniciar os servidores teremos 2 aplicações em  execução:

* _localhost:8080_ (host)
* _localhost:8081_ (dsl)

## Ferramentas utilizadas <img src="https://github.githubassets.com/images/icons/emoji/unicode/1f9f0.png" width="30px">

 * React como uma linguagem de IU
 * Webpack5 como module bundler
 * Prettier como formatador de código
 * Styled components como um gerenciador de estilos
 * TailwindCss UI como nosso kit de ferramentas de design




